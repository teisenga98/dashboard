#ifndef DATAMONITOR_H
#define DATAMONITOR_H

#include <vector>
#include <string>

#include "DataTransfer.h"
#include "Data.h"
#include "server.h"
#include "Timer.h"
#include "utilities.h"

#include <QDebug>

class DataMonitor : public QObject
{
    Q_OBJECT
public:
    // Constructor definition of DataMonitor
    DataMonitor(QObject *parent = nullptr);

    virtual ~DataMonitor();

    /**
     * Initialization definition of DataMonitor
     * @param fileName filename of the file where the demodata is stored
     * @param updateGraphsTimerTimeout updateGraphsTimer timeout in milliseconds
     * @return true when initialized correctly
    */
    bool initialize(std::string fileName, int updateGraphsTimerTimeout);

    /**
     * Start of the sytem
    */
    void start();

    /**
     * Pause of the sytem
    */
    void pause();

    /**
     * Stop of the sytem
    */
    void stop();

    /**
     * Get the initialized charts
     * @return Charts to be displayed on the ui
    */
    std::vector<QChart *> getSensorCharts() const { return sensorCharts; }

    /**
     * Get the amount of connected Sensorshorts
     * @return Number of connected Sensorshorts
    */
    int getConnectedsensorShortsAmount() { return utils->getConnectedSensorShortsAmount(); }

    /**
     * Get the index of the connected Sensorshorts
     * @return List of indexes of the connected Sensorshorts
    */
    std::vector<int> getConnectedSensorShorts() {return utils->getConnectedSensorShorts(); }

    /**
     * Get the system status
     * @return true if the the system started, false when stopped
    */
    bool getSystemStatus() { return systemStarted; }

signals:

    void systemStartedSignal(bool isSensorData);
    void systemPausedSignal(bool isSensorData);
    void systemStoppedSignal(bool isSensorData);

    void newSensorShortInitializedSignal();

public slots:
    // slot for selected short
    void shortSelected(int sensorNr);

    void newSensorShortConnectedSlot() { emit newSensorShortInitializedSignal(); }

private slots:
    // this slot is used to determine the timer has elapsed
    void updateGraphsTimerSlot();

    // slot for incoming new data
    void newDataPacketArraySlot(std::vector<Data> packetArrayIn);

private:

    /**
     * Check if the calculated sensorvalue if above the treshold
     * @param euclideanIn Calculated Euclidean sensorvalue
     * @param index Index of the sensor to know what treshold to use
     * @return The QColor red if the treshold has been reached, otherwise QColor blue
    */
    QColor checkTreshold(double euclideanIn, int index);

    // timer for demo data graph update
    Timer *updateGraphsTimer;

    // datatransfer class object
    DataTransfer transfer;

    Utilities *utils;
    Server *server;

    // index for which data point to show
    uint16_t dataIndex;

    // max index of demodata dataset
    const int MaxDemoData = 5000;

    // counter for the amount of graphs to be generated
    int graphCount;

    // variable to know which short is needed to be displayed
    int selectedShort;

    // variable to know when the system is started
    bool systemStarted;

    // variable to know when the system is paused
    bool systemPaused;

    // variable to know when the system is stopped
    bool systemStopped;

    // number of runs the system has done
    int systemRunsNr;

    // filename base for the saved data file
    std::string filename;

    /** paired data vector
     * @param string these are the names of the sensor and axis
     * @param vector this vector holds the data for each sensor axis
    */
    std::vector<std::pair<std::string, std::vector<double>>> staticDataset;

    // vectors for different needed graphs
    std::vector<QBarSet*> sensorGraphs;
    std::vector<QChart*> sensorCharts;
    std::vector<QHorizontalBarSeries*> sensorSeries;

    int shortTimeout;
    int longTimeout;

    // min value of timeout
    const int MinTimeout = 100;
    const int ChartAmount = 9;

    // declare min and max values for the used charts
    // Euclidean min and max

    // in g
    const double MinAccValue = 0;
    const double MaxAccValue = 25.0;
    const double accValueTreshold = 20.0;

    // in degrees/s
    const double MinGyrValue = 0;
    const double MaxGyrValue = 2000.0;
    const double GyrValueTreshold = 1750.0;

    // in microtesla
    const double MinMagValue = 0;
    const double MaxMagValue = 200.0;
    const double MagValueTreshold = 150.0;

    const int trunkAccVecLocation = 0;
    const int leftLegAccVecLocation = 3;
    const int rightLegAccVecLocation = 6;

    const int trunkGyrVecLocation = 1;
    const int leftLegGyrVecLocation = 4;
    const int rightLegGyrVecLocation = 7;

    const int trunkMagVecLocation = 2;
    const int leftLegMagVecLocation = 5;
    const int rightLegMagVecLocation = 8;

    const int TeslaMicroTeslaConversion = 1000000;

    // charts to sensors in vector:
    // 0 = trunkAcc
    // 1 = trunkGyr
    // 2 = trunkMag
    // 3 = leftLegAcc
    // 4 = leftLegGyr
    // 5 = leftLegMag
    // 6 = rightLegAcc
    // 7 = rightLegGyr
    // 8 = rightLegMag

    // sensor 31 = trunk (romp)
    // sensor 11 = left leg
    // sensor 21 = right leg
};

#endif
