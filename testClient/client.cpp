/*
   client.cpp

   Test client for the tcpsockets classes. 

   ------------------------------------------

   Copyright (c) 2013 Vic Hargrave

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include "tcpconnector.h"

#include <chrono>
#include <thread>

#include <bitset>
using namespace std;
#include <iostream>
int main(int argc, char** argv)
{
    if (argc != 3) {
        printf("usage: %s <port> <ip>\n", argv[0]);
        exit(1);
    }

//    int len;
    string message;
//    char line[256];
    int i = 0;
    TCPConnector* connector = new TCPConnector();
    TCPStream* stream = connector->connect(argv[2], atoi(argv[1]));
    if (stream) {

        // while (i < 100)
        // {
            // message = "{{Dfvboewinviowev}}";
            uint8_t buff[] = {'{', '{', 'I', 74, 0, 98, 0, 19, 80, 78, 48, 75, 54, 49, 32, '}', '}'};
            uint8_t buff2[] = {'{', '{', 'D', 74, 0, 98, 0, 19, 80, 78, 48, 75, 54, 74, 0, 98, 0, 19, 80, 78, 48, 75, 54, 74, 0, 98, 0, 19, 80, 78, 48, 75, 54, 74, 0, 98, 0, 19, 80, 78, 48, 75, 54, 74, 0, 98, 0, 19, 80, 78, 48, 75, 54, 74, 0, 98, 0, 19, 80, 78, 48, 75, 54, 74, 0, 98, 0, 19, 80, 78, 48, 75, 54, 74, 0, 98, 0, 19, 80, 78, 48, 75, 54, 74, 0, 98, 0, 19, 80, 78, 48, 75, 54, '}', '}'};
            uint8_t buff3[] = {'{', '{', 'D', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '}', '}'};
            std::cout << "sizeof buff: " << sizeof(buff2) << std::endl;
            for(int i = 0;i<17;i++)
            {
                std::bitset<8> x(buff[i]);
                std::cout << x;
                // printf("buff: %x\n", ~buff[i]);
                // std::cout << buff[i] << std::endl;
            }
            std::cout << std::endl;
            stream->send((char*)buff,17);
            // printf("sent - %s\n", message.c_str());
            // std::this_thread::sleep_for(std::chrono::milliseconds(50));
            // i++;
        // }
        std::this_thread::sleep_for(std::chrono::seconds(20));
        while(true) {
            stream->send((char*)buff2, 95);
            std::this_thread::sleep_for(std::chrono::milliseconds(40));
        }
        // 100110011001100110011001100110011001100110011001100110011001100110011001100110011001100110011001
//        len = stream->receive(line, sizeof(line));
//        line[len] = 0;
//        printf("received - %s\n", line);
        delete stream;
    }

//     stream = connector->connect(argv[2], atoi(argv[1]));
//     if (stream) {
//         message = "Why is there air?";
//         stream->send(message.c_str(), message.size());
//         printf("sent - %s\n", message.c_str());
// //        len = stream->receive(line, sizeof(line));
// //        line[len] = 0;
// //        printf("received - %s\n", line);
//         delete stream;
//     }
    exit(0);
}

// 123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
