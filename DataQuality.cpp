#include <array>
#include <chrono>
#include <memory>
#include <QDebug>


#include "DataQuality.h"

DataQuality::DataQuality(std::string routerIPAdress) :
    initialized(false),
    executeCommandTimer(new Timer()),
    connectedShorts(0),
    routerIP(routerIPAdress)
{
    // set the timer timeout
    executeCommandTimer->setTimeout(TimerTimeout);

    // set the command with the correct IP adress
    command = "ssh -o ConnectTimeout=1 root@" + routerIP + ScriptToExecute;

    // connect timer timeout signal to executecommandtimerslot
    QObject::connect(executeCommandTimer, SIGNAL(timeoutSignal()), this, SLOT(executeCommandTimerSlot()));
}

DataQuality::~DataQuality()
{
    // only delete graph if it is initialized at the first place
    if (initialized) {
        for (uint i = 0; i < connectedShorts; i++) {
            delete signalStrenghtSeries.at(i);
            signalStrenghtSeries.at(i) = nullptr;
        }
    }

    executeCommandTimer->stop();

    delete executeCommandTimer;
    executeCommandTimer = nullptr;
}

void DataQuality::startSignalStrengthData(uint connectedShortsIn)
{
    createSignalStrenghtgraph(connectedShortsIn);
    executeCommandTimer->start();
}

void DataQuality::createSignalStrenghtgraph(uint connectedShortsIn)
{
    initialized = true;
    connectedShorts = connectedShortsIn;
    for (uint i = 0; i < connectedShorts; i++) {
        signalStrenghtSeries.push_back(new QSplineSeries());
    }

    signalStrenghtGraph = new QChart();
    signalStrenghtAxisX = new QValueAxis();
    signalStrenghtAxisY = new QValueAxis();

    signalStrenghtXValue = 5;
    signalStrenghtYValue = 0;

    QPen pen(Qt::red);
    pen.setWidth(3);

    for (uint i = 0; i < connectedShorts; i++) {
        signalStrenghtSeries.at(i)->setPen(colorList.at(i));
    }

    // set axis pixel size
    QFont labelFont;
    labelFont.setPixelSize(11);

    for (uint i = 0; i < connectedShorts; i++) {
        signalStrenghtSeries.at(i)->append(signalStrenghtXValue, signalStrenghtYValue);
    }

    for (uint i = 0; i < connectedShorts; i++) {
        signalStrenghtGraph->addSeries(signalStrenghtSeries.at(i));
    }

    signalStrenghtGraph->addAxis(signalStrenghtAxisX, Qt::AlignBottom);
    signalStrenghtGraph->addAxis(signalStrenghtAxisY, Qt::AlignLeft);

    for (uint i = 0; i < connectedShorts; i++) {
        signalStrenghtSeries.at(i)->attachAxis(signalStrenghtAxisX);
        signalStrenghtSeries.at(i)->attachAxis(signalStrenghtAxisY);
    }

    signalStrenghtAxisX->setTickCount(5);

    signalStrenghtAxisX->setLabelsFont(labelFont);
    signalStrenghtAxisY->setLabelsFont(labelFont);

    signalStrenghtAxisX->setRange(0, 10);
    signalStrenghtAxisY->setRange(-90, -20);
    signalStrenghtGraph->legend()->hide();
    signalStrenghtAxisX->setVisible(false);

}

// from https://stackoverflow.com/questions/478898/how-do-i-execute-a-command-and-get-the-output-of-the-command-within-c-using-po
std::string DataQuality::executeCommand()
{
    std::array<char, 512> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(command.c_str(), "r"), pclose);

    if (!pipe) {
        qDebug() << "popen() failed!";
    }
    else {
        while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
            result += buffer.data();
        }
    }
    return result;
}

void DataQuality::executeCommandTimerSlot()
{
    // execute command and get the output
    std::string output = executeCommand();

    if (!output.empty()) {

        // check how many outputs are given by the command using search of \n
        std::vector<size_t> positions;
        std::vector<std::string> macAdresses;
        std::vector<int> signalStrengths;

        size_t pos = output.find('\n', 0);
        while (pos != std::string::npos) {
            positions.push_back(pos);

            pos = output.find('\n', pos + 1);
        }

        // calculate connected devices using found '\n', each connected device has 2 so / 2
        int connectedDevices = positions.size() / 2;

        // save and remove all mac adresses from connected devices
        for (int i = 0; i < connectedDevices; i++) {

            // find '\n' in output string
            size_t enterPos = output.find('\n');

            // get mac adress in string format
            std::string tmp = output.substr(0, enterPos);

            // add mac adress to vector
            macAdresses.push_back(tmp);

            // remove added mac adress and '\n'
            output.erase(0, enterPos + 1);
        }

        // save all signal strengths from connected devices
        for (int i = 0; i < connectedDevices; i++) {

            // find '\n' in output string
            size_t enterPos = output.find('\n');

            // get signal strenght in string format
            std::string tmp = output.substr(0, enterPos);

            // convert and add signal strength to vector
            signalStrengths.push_back(stoi(tmp));

            // remove added signal strength and '\n'
            output.erase(0, enterPos + 1);

        }
        // if there are signal strength to display, display it
        if (!signalStrengths.empty()) {
            qreal x = signalStrenghtGraph->plotArea().width() / signalStrenghtAxisX->tickCount();
            qreal y = (signalStrenghtAxisX->max() - signalStrenghtAxisX->min()) / signalStrenghtAxisX->tickCount();

            signalStrenghtXValue += y;

            // only update data if all data has been received
            if (signalStrengths.size() == connectedShorts) {
                for (uint i = 0; i < connectedShorts; i++) {
                    signalStrenghtYValue = signalStrengths.at(i);
                    signalStrenghtSeries.at(i)->append(signalStrenghtXValue, signalStrenghtYValue);
                }
            }
            signalStrenghtGraph->scroll(x, 0);
        }

    }
}
