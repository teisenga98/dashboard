#include "Timer.h"
//#include "DataMonitor.h"

#include <QDebug>

Timer::Timer(QObject *parent):
    QObject(parent),
    ms(1000)
{
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &Timer::handleTimeout);
}

Timer::~Timer()
{
    delete timer;
    timer = nullptr;
}

void Timer::start()
{
    timer->start();
}

void Timer::stop()
{
    timer->stop();
}

void Timer::handleTimeout()
{
    emit timeoutSignal();
}
