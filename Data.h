#ifndef DATA_H
#define DATA_H

#include <QtCharts>
/**
 * Data structure of 1 set of Sensorshorts data
 * This structure is initialized a maximum of 22 times where each initalization is a new Sensorshorts
 * @param dataID ID of the Sensorshorts
 * @param socketID SocketDescriptor ID of the connected Sensorshorts
 * @param stm32ID Unique stm32 ID of of the connected Sensorshorts
 * @param rawDataPacket datapacket where the raw Sensorshorts data is stored
 * @param dataPacket Datapacket where the converted Sensorshorts data is stored
 */
struct Data
{
    int dataID;
    quint8 socketID;
    std::string sensorShortName;
    std::vector<quint8> stm32ID;
    std::vector<std::pair<std::string, std::vector<qint16>>> rawDataPacket;
    std::vector<std::pair<std::string, std::vector<double>>> dataPacket;
};

// Names of the different sensors used
const std::vector<std::string> SensorNames = {"acc-x-31", "acc-y-31", "acc-z-31", "gyr-x-31", "gyr-y-31", "gyr-z-31",
                                              "mag-x-31", "mag-y-31", "mag-z-31", "acc-x-11", "acc-y-11", "acc-z-11",
                                              "gyr-x-11", "gyr-y-11", "gyr-z-11", "mag-x-11", "mag-y-11", "mag-z-11",
                                              "acc-x-21", "acc-y-21", "acc-z-21", "gyr-x-21", "gyr-y-21", "gyr-z-21",
                                              "mag-x-21", "mag-y-21", "mag-z-21", "acc-x-12", "acc-y-12", "acc-z-12",
                                              "gyr-x-12", "gyr-y-12", "gyr-z-12", "mag-x-12", "mag-y-12", "mag-z-12",
                                              "acc-x-22", "acc-y-22", "acc-z-22", "gyr-x-22", "gyr-y-22", "gyr-z-22",
                                              "mag-x-22", "mag-y-22", "mag-z-22"}; // "time1", "time2", "movement"

/**
 * Combination of the Sensorshorts number on the physical shorts and the Unique stm32 ID
 * This is used to show the correct Sensorshorts number on the display and to initialze the correct shorts
 * @param int dataID of Data structure
 * @param vector Unique stm32 ID that has to match with the incoming ID
 */
const std::vector<std::pair<int, std::vector<quint8>>> sensorShortNrIDCombo = { {1, {}},
                                                                                {2, {}},
                                                                                {3, {}},
                                                                                {4, {73, 0, 45, 0, 19, 80, 78, 48, 75, 54, 49, 32} },
                                                                                {5, {} },
                                                                                {6, {} },
                                                                                {7, {74, 0, 98, 0, 19, 80, 78, 48, 75, 54, 49, 32} },
                                                                                {8, {} },
                                                                                {9, {} },
                                                                                {10, {} },
                                                                                {11, {} },
                                                                                {12, {} },
                                                                                {13, {} },
                                                                                {14, {} },
                                                                                {15, {} },
                                                                                {16, {} },
                                                                                {17, {} },
                                                                                {18, {} },
                                                                                {19, {} },
                                                                                {20, {} },
                                                                                {21, {} },
                                                                                {22, {} } };

#endif // DATA_H
