#ifndef EVENTS_H
#define EVENTS_H

/*
 * Event enum for all the events that can occur in this program.
 * @short_timeout = 100 milliseconds
 * @long_timeout = 1000 milliseconds
*/
enum class Events {
    no_event,
    short_timeout,
    long_timeout

};

#endif // EVENTS_H
