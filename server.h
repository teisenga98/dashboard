/* from https://stackoverflow.com/questions/20546750/qtcpsocket-reading-and-writing */

#ifndef SERVER_H
#define SERVER_H

#include <QtCore>
#include <QtNetwork>

class Server : public QObject
{
    Q_OBJECT
public:
    explicit Server(QObject *parent = nullptr);

    void sendMessage(std::string message);

signals:
    void dataReceived(quint8 socketID, QByteArray a);

    void removeShortFromInitializedList(quint8 socketID);

public slots:
    void run();

    void start();
    void stop();

private slots:
    void newConnection();
    void disconnected();
    void readyRead();

private:
    QTcpServer *server;
    QHash<QTcpSocket*, QByteArray*> buffers; //We need a buffer to store data until block has completely received
    QHash<QTcpSocket*, qint32*> sizes; //We need to store the size to verify if a block has received completely

    QList<QTcpSocket*> socketList;

    std::vector<QByteArray> messageVector;
};
#endif // SERVER_H
