#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <chrono>

#include "DataMonitor.h"
#include "DataQuality.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void setConnectedsensorShorts();

signals:
    void shortSelected(int sensorNr);

public slots:
    void systemStartedSlot(bool isSensorData);
    void systemPausedSlot(bool isSensorData);
    void systemStoppedSlot(bool isSensorData);

    void newSensorShortInitializedSlot();

private slots:
    void on_shortSelectSpinBox_valueChanged(int value);

    void on_startButton_clicked();

    void on_stopButton_clicked();

    void on_downloadDataButton_clicked();

    void on_pauseButton_clicked();

    void on_sensorShortSelectComboBox_currentIndexChanged(const QString &arg1);

private:
    QString formatTime(std::chrono::milliseconds ms);

    Ui::MainWindow *ui;

    DataMonitor *monitor;

    DataQuality *quality;

    int retConnected;

    bool firstStart;

    const std::vector<QString> sensorNamesEuclidean = {"acc-31*", "gyr-31*", "mag-31*",
                                                           "acc-11*", "gyr-11*", "mag-11*",
                                                           "acc-21*", "gyr-21*", "mag-21*"};

};

#endif // MAINWINDOW_H
