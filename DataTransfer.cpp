#include "DataTransfer.h"

std::vector<std::pair<std::string, std::vector<double>>> DataTransfer::readData(std::string fileName)
{
    // data out variable
    std::vector<std::pair<std::string, std::vector<double>>> dataOut;

    // if parameter is invalid, return empty std::vector
    if (fileName.empty())
    {
        return dataOut;
    }

    // get raw data from file
    std::vector<int16_t> rawData = getRawData(fileName);

    // check if raw data is valid
    if (rawData.empty())
    {
        return dataOut;
    }

    // calculate size of data
    size_t dataSize = (rawData.size() - OffSet) / sensorNames.size();

    // convert raw data to actual data
    dataOut = convertRawData(dataSize, rawData);

    // return the converted data
    return dataOut;
}

// from https://www.gormanalysis.com/blog/reading-and-writing-csv-files-with-cpp/
std::vector<std::pair<std::string, std::vector<double>>> DataTransfer::read_csv(std::ifstream &str)
{
    // Reads a CSV file into a vector of <string, vector<int>> pairs where
    // each pair represents <column name, column values>

    // Create a vector of <string, int vector> pairs to store the result
    std::vector<std::pair<std::string, std::vector<double>>> result;

    // Make sure the file is open
    if (!str.is_open())
        throw std::runtime_error("Could not open file");

    // Helper vars
    std::string line, colname;
    double val;

    // Read the column names
    if (str.good())
    {
        // Extract the first line in the file
        std::getline(str, line);

        // Create a stringstream from line
        std::stringstream ss(line);

        // Extract each column name
        while (std::getline(ss, colname, ';'))
        {

            // Initialize and add <colname, int vector> pairs to result
            result.push_back({colname, std::vector<double>{}});
        }
    }

    // Read data, line by line
    while (std::getline(str, line))
    {
        // Create a stringstream of the current line
        std::stringstream ss(line);

        // Keep track of the current column index
        int colIdx = 0;

        // Extract each integer
        while (ss >> val)
        {

            // Add the current integer to the 'colIdx' column's values vector
            result.at(colIdx).second.push_back(val);

            // If the next token is a comma, ignore it and move on
            if (ss.peek() == ';')
                ss.ignore();

            // Increment the column index
            colIdx++;
        }
    }
    return result;
}

void DataTransfer::write_csv_header(std::string filename, std::vector<std::string> dataset)
{
    // Make a CSV file with one or more columns of integer values
    // Each column of data is represented by the pair <column name, column data>
    //   as std::pair<std::string, std::vector<double>>
    // The dataset is represented as a vector of these columns
    // Note that all columns should be the same size

    // Create an output filestream object
    std::ofstream myFile(filename, std::ofstream::app);

    // Send column names to the stream
    for(uint j = 0; j < dataset.size(); ++j)
    {
        myFile << dataset.at(j);
        if(j != dataset.size() - 1) {
            myFile << ","; // No comma at end of line
        }
    }
    myFile << "\n";

    myFile.close();
}

void DataTransfer::write_csv_data(std::string filenameConverted, std::string filenameint16Format, std::vector<Data> dataset, int selectedShort)
{
    // Make a CSV file with one or more columns of integer values
    // Each column of data is represented by the pair <column name, column data>
    //   as std::pair<std::string, std::vector<double>>
    // The dataset is represented as a vector of these columns
    // Note that all columns should be the same size

    // get the different data that needs to be written
    std::vector<std::pair<std::string, std::vector<double>>> convertedData = dataset.at(selectedShort).dataPacket;
    std::vector<std::pair<std::string, std::vector<qint16>>> int16FormatData = dataset.at(selectedShort).rawDataPacket;

    // Create an output filestream object
    std::ofstream fileConverted(filenameConverted, std::ofstream::app);
    std::ofstream fileint16Format(filenameint16Format, std::ofstream::app);

    // Send data to the correct stream
    for(uint i = 0; i < convertedData.at(0).second.size(); ++i)
    {
        for(uint j = 0; j < convertedData.size(); ++j)
        {
            fileConverted << convertedData.at(j).second.at(i);
            fileint16Format << int16FormatData.at(j).second.at(i);
            if(j != convertedData.size() - 1) {
                fileConverted << ","; // No comma at end of line
            }
            if(j != int16FormatData.size() - 1) {
                fileint16Format << ","; // No comma at end of line
            }
        }
        fileConverted << "\n";
        fileint16Format << "\n";
    }

    // Close the file
    fileConverted.close();
    fileint16Format.close();

}
std::vector<std::pair<std::string, std::vector<double>>> DataTransfer::convertRawData(size_t dataSize, std::vector<int16_t> rawData)
{
    // initialize matrix array and returnvalue std::vector
    long double data[dataSize][sensorNames.size()];
    std::vector<std::pair<std::string, std::vector<double>>> dataOut;

    // if parameters are invalid, return empty std::vector
    if (dataSize == 0 || rawData.empty())
    {
        return dataOut;
    }

    // loop trough all the datasamples to convert them
    for (size_t i = 0; i < dataSize; i++)
    {
        for (size_t j = 0; j < sensorNames.size(); j++)
        {
            // reformat raw data in matrix array for easier converting
            data[i][j] = rawData.at(i * sensorNames.size() + OffSet + j);
        }

        // TO-DO convert magic numbers

        // convert accelerometer raw data to real values
        data[i][0] = (data[i][0] / pow(2, 15)) * 32;
        data[i][1] = (data[i][1] / pow(2, 15)) * 32;
        data[i][2] = (data[i][2] / pow(2, 15)) * 32;

        data[i][9] = (data[i][9] / pow(2, 15)) * 32;
        data[i][10] = (data[i][10] / pow(2, 15)) * 32;
        data[i][11] = (data[i][11] / pow(2, 15)) * 32;

        data[i][18] = (data[i][18] / pow(2, 15)) * 32;
        data[i][19] = (data[i][19] / pow(2, 15)) * 32;
        data[i][20] = (data[i][20] / pow(2, 15)) * 32;

        // convert gyroscope raw data to real values
        data[i][3] = (data[i][3] / pow(2, 15)) * 4000;
        data[i][4] = (data[i][4] / pow(2, 15)) * 4000;
        data[i][5] = (data[i][5] / pow(2, 15)) * 4000;

        data[i][12] = (data[i][12] / pow(2, 15)) * 4000;
        data[i][13] = (data[i][13] / pow(2, 15)) * 4000;
        data[i][14] = (data[i][14] / pow(2, 15)) * 4000;

        data[i][21] = (data[i][21] / pow(2, 15)) * 4000;
        data[i][22] = (data[i][22] / pow(2, 15)) * 4000;
        data[i][23] = (data[i][23] / pow(2, 15)) * 4000;

        // convert magnetometer raw data to real values
        data[i][6] = data[i][6] * 0.15;
        data[i][7] = data[i][7] * 0.15;
        data[i][8] = data[i][8] * 0.15;

        data[i][15] = data[i][15] * 0.15;
        data[i][16] = data[i][16] * 0.15;
        data[i][17] = data[i][17] * 0.15;

        data[i][24] = data[i][24] * 0.15;
        data[i][25] = data[i][25] * 0.15;
        data[i][26] = data[i][26] * 0.15;

        data[i][33] = data[i][33] * 0.15;
        data[i][34] = data[i][34] * 0.15;
        data[i][35] = data[i][35] * 0.15;

        data[i][42] = data[i][42] * 0.15;
        data[i][43] = data[i][43] * 0.15;
        data[i][44] = data[i][44] * 0.15;
    }

    // add real values to correct formatted std::vector for making graphs
    for (size_t i = 0; i < sensorNames.size(); i++)
    {
        dataOut.push_back({sensorNames.at(i), std::vector<double>{}});
        for (size_t j = 0; j < dataSize; j++)
        {
            dataOut.at(i).second.push_back(data[j][i]);
        }
    }
    // return the correctly formatted std::vector with all converted IMU data
    return dataOut;
}

std::vector<int16_t> DataTransfer::getRawData(std::string fileName)
{
    // helper variables
    char *buffer;
    int length = 0;

    std::vector<int16_t> rawData;
    int16_t dataBuffer = 0x0;

    // if parameter is invalid, return empty std::vector
    if (fileName.empty())
    {
        return rawData;
    }

    // get raw data from file
    std::ifstream file(fileName, std::ifstream::binary);

    // check if file is valid
    if (!file.good())
    {
        return rawData;
    }

    // go to end of file
    file.seekg(0, file.end);

    // get reading position aka character count
    length = file.tellg();

    // go back to beginning of file for reading
    file.seekg(0, file.beg);

    // create buffer size of file
    buffer = new char[length];

    // read raw data
    file.read(buffer, length);

    int combineIndex = 0;
    for (size_t i = 0; i < 500000; i++) // TO-DO change this value to real value
    {
        /** convert the 8 bit data to the needed 16 bit data size
         *  each piece of data consists of a pair of 2 8 bit raw data
         *  this is combined using a bitwise or operation with the second raw data shifted
         */

        dataBuffer = (((uint8_t)buffer[combineIndex] | ((uint8_t)buffer[combineIndex + 1] << 8)));

        // add combined data to raw buffer
        rawData.push_back(dataBuffer);

        // go to next pair of data
        combineIndex += 2;
    }

    // delete needed buffer to free memory
    delete[] buffer;
    buffer = nullptr;

    return rawData;
}
