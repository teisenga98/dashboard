#ifndef DATAQUALITY_H
#define DATAQUALITY_H

#include <QObject>
#include <QtCharts>
#include <string>

#include "Timer.h"



class  DataQuality : public QObject
{
    Q_OBJECT
public:
    DataQuality(std::string routerIPAdress);
    ~DataQuality();

    QChart* getSignalStrengthGraph() { return signalStrenghtGraph; }

    void startSignalStrengthData(uint connectedShortsIn);

private slots:

    void executeCommandTimerSlot();

private:

    bool initialized;

    void createSignalStrenghtgraph(uint connectedShortsIn);

    std::string executeCommand();


    QChart* signalStrenghtGraph;

    std::vector<QSplineSeries*> signalStrenghtSeries;

    QValueAxis *signalStrenghtAxisX;
    QValueAxis *signalStrenghtAxisY;

    qreal step;
    qreal signalStrenghtXValue;
    qreal signalStrenghtYValue;

    Timer *executeCommandTimer;

    uint connectedShorts;

    std::string command;

    std::string routerIP;

    // script to execute on the raspberry pi router with openwrt
    const std::string ScriptToExecute = " \"./show_wifi_clients.sh\"";

    // set timer timeout to 3 seconds (include the wait time for command execution = 5 seconds total
    const int TimerTimeout = 3000;

    // graph color list
    std::vector<QColor> colorList = {Qt::green, Qt::blue, Qt::cyan,
                                     Qt::magenta, Qt::yellow, Qt::darkRed,
                                     Qt::darkGreen, Qt::darkBlue, Qt::darkCyan,
                                     Qt::darkMagenta, Qt::darkYellow, Qt::black,
                                     Qt::white, Qt::darkGray, Qt::gray, Qt::lightGray,
                                     Qt::red, QColor(0, 255, 64), QColor(191, 0, 255),
                                     QColor(255, 128, 0), QColor(255, 128, 128), QColor(255, 0, 128) };
};

#endif // DATAQUALITY_H
