#include "Mainwindow.h"
#include "ui_Mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent):
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    retConnected(-1),
    firstStart(true)
{
    ui->setupUi(this);

    // check which platform is being used to use the correct file path
#ifdef Q_PROCESSOR_ARM
    std::string filePath = "/home/pi/Documents/RUN_1.TXT";
#else
    std::string filePath = "/home/ubuntu/Downloads/RUN_1.TXT";
#endif


//    // check if system is connected to openwrt modem using pinging the modem (uncomment this if used)
//    retConnected = system("ping -W 1 -c 1 192.168.2.1");

//    // if system is connected to moden create dataquality object and initialize its graphs
//    if (retConnected == 0) {
//        quality = new DataQuality("192.168.2.1");
//    }

    // remove excisting saved files on Raspberry Pi
    system("cd && sudo rm runs/*");

    // initialize DataMonitor
    monitor = new DataMonitor();

    QObject::connect(this, &MainWindow::shortSelected, monitor, &DataMonitor::shortSelected);

    // initialize Datamonitor and check for initialization errors
    if (!monitor->initialize(filePath, 100)) {
        qDebug() << "Initialization failed, please check parameters";
        exit(EXIT_FAILURE);
    }

    // attach the correct charts to the graphicviews
    std::vector<QChart*> sensorCharts =  monitor->getSensorCharts();

    for (uint i = 0; i < sensorCharts.size(); i++) {

        sensorCharts.at(i)->setAnimationOptions(QChart::AllAnimations);
    }

    // attach left leg chats to ui (left leg is 11)
    ui->leftLegAcc->setChart(sensorCharts.at(3));
    ui->leftLegGyr->setChart(sensorCharts.at(4));
    ui->leftLegMag->setChart(sensorCharts.at(5));

    // attach trunk leg chats to ui (trunk is 31)
    ui->trunkAcc->setChart(sensorCharts.at(0));
    ui->trunkGyr->setChart(sensorCharts.at(1));
    ui->trunkMag->setChart(sensorCharts.at(2));  

    // attach right leg chats to ui (right leg is 21)
    ui->rightLegAcc->setChart(sensorCharts.at(6));
    ui->rightLegGyr->setChart(sensorCharts.at(7));
    ui->rightLegMag->setChart(sensorCharts.at(8));

    // attach names to correct labels
    ui->leftLegAccLabel->setText(sensorNamesEuclidean.at(3));
    ui->leftLegGyrLabel->setText(sensorNamesEuclidean.at(4));
    ui->leftLegMagLabel->setText(sensorNamesEuclidean.at(5));

    ui->trunkAccLabel->setText(sensorNamesEuclidean.at(0));
    ui->trunkGyrLabel->setText(sensorNamesEuclidean.at(1));
    ui->trunkMagLabel->setText(sensorNamesEuclidean.at(2));

    ui->rightLegAccLabel->setText(sensorNamesEuclidean.at(6));
    ui->rightLegGyrLabel->setText(sensorNamesEuclidean.at(7));
    ui->rightLegMagLabel->setText(sensorNamesEuclidean.at(8));

    ui->sensorShortSelectComboBox->view()->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);

    ui->connectedsensorShortslabel->setText("Connected shorts: " + QString::number(monitor->getConnectedsensorShortsAmount()));

    QObject::connect(monitor, &DataMonitor::systemStartedSignal, this, &MainWindow::systemStartedSlot);

    QObject::connect(monitor, &DataMonitor::systemPausedSignal, this, &MainWindow::systemPausedSlot);

    QObject::connect(monitor, &DataMonitor::systemStoppedSignal, this, &MainWindow::systemStoppedSlot);

    QObject::connect(monitor, &DataMonitor::newSensorShortInitializedSignal, this, &MainWindow::newSensorShortInitializedSlot);

}

MainWindow::~MainWindow()
{
    delete ui;
    ui = nullptr;

    delete monitor;
    monitor = nullptr;

    // remove dataquality object if initialized
    if (retConnected == 0) {
        delete quality;
        quality = nullptr;
    }
}

void MainWindow::newSensorShortInitializedSlot()
{
    // get connected shorts and update short selection list on display
    std::vector<int> tmpConnectedShorts = monitor->getConnectedSensorShorts();
    ui->sensorShortSelectComboBox->clear();
    for (uint i = 0; i < tmpConnectedShorts.size(); i++) {
        ui->sensorShortSelectComboBox->addItem(QString::number(tmpConnectedShorts.at(i)));
    }

    ui->connectedsensorShortslabel->setText("Connected shorts: " + QString::number(monitor->getConnectedsensorShortsAmount()));
}

void MainWindow::systemStartedSlot(bool isSensorData)
{
    QString tmp = "System started ";
    tmp.append(isSensorData ? "(sensorshortdata)" : "(demodata)");
    ui->timeRemainingLabel->setText(tmp);
}

void MainWindow::systemPausedSlot(bool isSensorData)
{
    QString tmp = "System paused ";
    tmp.append(isSensorData ? "(sensorshortdata)" : "(demodata)");
    ui->timeRemainingLabel->setText(tmp);
}

void MainWindow::systemStoppedSlot(bool isSensorData)
{
    QString tmp = "System stopped ";
    tmp.append(isSensorData ? "(sensorshortdata)" : "(demodata)");
    ui->timeRemainingLabel->setText(tmp);
}

void MainWindow::on_shortSelectSpinBox_valueChanged(int value)
{
    emit shortSelected(value);
}

void MainWindow::on_sensorShortSelectComboBox_currentIndexChanged(const QString &arg1)
{
    emit shortSelected(arg1.toInt());
    qDebug() << "arg1 of combobox: " << arg1.toInt();
}

QString MainWindow::formatTime(std::chrono::milliseconds ms)
{
    auto seconds = std::chrono::duration_cast<std::chrono::seconds>(ms);
    ms -= std::chrono::duration_cast<std::chrono::milliseconds>(seconds);

    auto minutes = std::chrono::duration_cast<std::chrono::minutes>(seconds);
    seconds -= std::chrono::duration_cast<std::chrono::seconds>(minutes);

    std::stringstream ss;
    ss << minutes.count() << ":" << seconds.count();

    return QString::fromStdString(ss.str());
}

void MainWindow::on_startButton_clicked()
{
    // uncomment this if openwrt moden is used
//    if (retConnected == 0 && firstStart) {

//        firstStart = false;

//        quality->startSignalStrengthData(monitor->getConnectedsensorShortsAmount());

//        QChart* chart = quality->getSignalStrengthGraph();
//        chart->setAnimationOptions(QChart::AllAnimations);
//        ui->signalStrenghtChartView->setRenderHint(QPainter::Antialiasing);
//        ui->signalStrenghtChartView->setChart(chart);
//    }
    monitor->start();
}

void MainWindow::on_pauseButton_clicked()
{
    monitor->pause();
}

void MainWindow::on_stopButton_clicked()
{
    monitor->stop();
}

void MainWindow::on_downloadDataButton_clicked()
{
    // create different class initalizations needed for QDialog
    QDialog *errorBox = new QDialog();
    QVBoxLayout *vLayout = new QVBoxLayout();
    QLabel *label = new QLabel("Error opening USB device\nplease check connection");
    QAbstractButton *exitButton = new QPushButton("Close");
    QFont font;

    // set QDialog size
    errorBox->setFixedSize(300, 200);

    // set QDialog style
    errorBox->setStyleSheet("QDialog { background-color : grey; color : white; }");

    // set font size and bold true
    font.setPointSize(6);
    font.setBold(true);

    // add font to text boxes
    label->setFont(font);
    exitButton->setFont(font);

    // set stylesheet of text boxes
    label->setStyleSheet("QLabel { color : white; }");
    exitButton->setStyleSheet("QAbstractButton { background-color : rgb(85, 87, 83); color : white; }");

    // add widgets to layout
    vLayout->addWidget(label);
    vLayout->addWidget(exitButton);

    // set the QDialog layout
    errorBox->setLayout(vLayout);

    // add clicked signal of exitbutton to closing of QDialog
    errorBox->connect(exitButton, SIGNAL(clicked()), errorBox, SLOT(close()));

    // read usb device status
    int ret = system("ls /dev/sda");

    // check if usb device is plugged in
    if (ret == 0) {
        // only allow data to be downloaded after the system has stopped
        if (!monitor->getSystemStatus()) {

            // mount the usb device
            ret = system("sudo mount -t vfat /dev/sda1 /media/usbstick");

            // copy the run files to the usb stick
            ret = system("sudo cp runs/RUN* /media/usbstick");

            // unmount the usb device
            ret = system("sudo umount /dev/sda1");

            // set label text
            label->setText("download complete!\nyou may now unmount\nthe USB device");
        }
        else {
            // set label text
            label->setText("Please stop the system\nbefore downloading data");
        }

        // finally show QDialog with correct label
        errorBox->show();
    }

    // if usb device is not plugged in, create QDialog to let the user know
    else {

        // set label text
        label->setText("Error opening USB device\nplease check connection");

        // finally show QDialog
        errorBox->show();
    }
}
