#include <qdebug.h>


#include "ProtocolCommands.h"
#include "utilities.h"

Utilities::Utilities(QObject *parent):
    QObject(parent),
    startIdentified(false),
    startHalfIdentified(false),
    endIdentified(false),
    sensorShortsAmount(0)
{
    std::vector<std::pair<std::string, std::vector<double>>> tmpDataPacket;
    std::vector<std::pair<std::string, std::vector<qint16>>> tmpRawDataPacket;
    for (size_t i = 0; i < SensorNames.size(); i++) {
        tmpDataPacket.push_back({SensorNames.at(i), {}});
        tmpRawDataPacket.push_back({SensorNames.at(i), {}});
    }
    for (quint8 i = 0; i < MaxSensorShorts; i++) {

        dataPacketArray.push_back(Data{(i + 1),
                                       0,
                                       SensorSHortName + std::to_string(i),
                                       {},
                                       tmpRawDataPacket,
                                       tmpDataPacket
                                  });
    }
}

int Utilities::getConnectedSensorShortsAmount()
{
    int connectedShorts = 0;
    for (uint i = 0; i < connectedSensorShorts.size(); i++) {

        // check if a sensorshort is connected and initialized to the program
        if (connectedSensorShorts.at(i).second == true) {

            // if a sensorshort is connected count up
            connectedShorts++;
        }
    }
    return connectedShorts;
}

// return the indexes (sensorshortNr) of all the connected sensorshorts
std::vector<int> Utilities::getConnectedSensorShorts()
{
    std::vector<int> tmpShortsList;

    for (uint i = 0; i < connectedSensorShorts.size(); i++) {

        // check if a sensorshort is connected and initialized to the program
        if (connectedSensorShorts.at(i).second) {

            // add connected shortNr to vector
            tmpShortsList.push_back(connectedSensorShorts.at(i).first);
        }
    }
    return tmpShortsList;
}

void Utilities::removeShortFromList(quint8 socketID) // TODO
{
    for (uint i = 0; i < dataPacketArray.size(); i++) {
        if (dataPacketArray.at(i).socketID == socketID) {
            dataPacketArray.at(i).socketID = 0;
            dataPacketArray.at(i).stm32ID.clear();
        }
    }
}

void Utilities::checkValidMessage(quint8 socketID, QByteArray message)
{
    for (int i = 0; i < message.size(); i++) {

        // check if start identifiers have been found
        if (!startIdentified) {
            if (message.at(i) == StartIdentifier) {

                // if first '{' was found in previous message the start is identified
                if (startHalfIdentified) {

                    // set variables
                    startIdentified = true;
                    startHalfIdentified = false;
                    endIdentified = false;

                    // skip identifier
                    i += 1;
                }

                // check if at the end of message
                else if ((i < message.size() - 1)) {

                    // check for second '{' identifier
                    if (message.at(i + 1) == StartIdentifier) {

                        // set variables
                        startIdentified = true;
                        endIdentified = false;

                        // skip identifier
                        i += 1;
                    }
                }

                // if first '{' identifier has been found but message has ended
                else {
                    startHalfIdentified = true;
                }
            }

            // if other data comes in between the message identifiers set the half identified false
            else {
                startHalfIdentified = false;
            }
        }

        // if start has been identified start saving data
        else {

            // check if end identifiers have been found
            if (!endIdentified) {

                // add data to dataPacket and make sure it is of type uint8
                dataPacket.push_back((quint8)message.at(i));

                if (message.at(i) == EndIdentifier) {
                    if (endHalfIdentified) {

                        // if end was half found before, remove last 2 characters (both are end identifiers
                        dataPacket.pop_back();
                        dataPacket.pop_back();

                        // check what kind of message it is and add it to the correct main vector
                        checkMessageType(socketID, dataPacket);

                        // remove dataPacket content for next piece of data
                        dataPacket.clear();

                        // reset variables
                        endIdentified = true;
                        endHalfIdentified = false;
                        startIdentified = false;
                    }

                    // check if at the end of message
                    else if ((i < message.size() - 1)) {

                        // check for second '}' identifier
                        if (message.at(i + 1) == EndIdentifier) {

                            // if end was found, remove end identifier
                            dataPacket.pop_back();

                            // check what kind of message it is and add it to the correct main vector
                            checkMessageType(socketID, dataPacket);

                            // remove dataPacket content for next piece of data
                            dataPacket.clear();

                            // reset variables
                            endIdentified = true;
                            startIdentified = false;
                        }
                    }

                    // if first '}' identifier has been found but message has ended
                    else {
                        endHalfIdentified = true;
                    }
                }

                // if other data comes in between the message identifiers set the half identified false
                else {
                    endHalfIdentified = false;
                }
            }
        }
    }
}

void Utilities::checkMessageType(quint8 socketID, std::vector<quint8> dataPacket)
{
    std::vector<qint16> rawDataPacket;
    std::vector<double> convertedDataPacket;
    bool alreadyInitialized = false;

    if (dataPacket.front() == InitIdentifier) {

        // remove message type identifier
        dataPacket.erase(dataPacket.begin());

        // if max size not reached
        if (sensorShortsAmount < MaxSensorShorts) {

            for (uint i = 0; i < dataPacketArray.size(); i++) {

                // check if incoming socketID matched an already initialized sensorshort
                if (dataPacketArray.at(i).socketID == socketID) {

                    // this short has been initialized before, do not re add it to the list
                    alreadyInitialized = true;

                    break;
                }
            }

            // if the short has not been initialized before add new socketID
            if (!alreadyInitialized) {

                // check if a new sensorshort is in the already known list of shorts
                for (uint i = 0; i < connectedSensorShorts.size(); i++) {

                    // if stm32ID matches a known ID set it connection state true
                    if (sensorShortNrIDCombo.at(i).second == dataPacket) {

                        // set connection state of short with mathing number true
                        connectedSensorShorts.at(i).second = true;

                        // add socketID to Data struct packet vector
                        dataPacketArray.at(i).socketID = socketID;

                        // add stm32ID (unique ID) to Data struct packet vector
                        dataPacketArray.at(i).stm32ID = dataPacket;

                        // update amount
                        sensorShortsAmount++;

                        emit newSensorShortInitialized();
                        // short initialized, no need to continue loop
                        break;
                    }
                }
            }
        }
        // if max size reached don't add another sensorshort
        else {
            qDebug() << "max amount of sensorshorts reached";
        }
    }
    else if (dataPacket.front() == DataIdentifier) {

        // remove message type identifier
        dataPacket.erase(dataPacket.begin());

        for (uint i = 0; i < dataPacketArray.size(); i++) {

            // check if incoming socketID matched an already initialized sensorshort
            if (dataPacketArray.at(i).socketID == socketID) {

                // convert 8 bit raw data to 16 bit raw data
                rawDataPacket = convert8BitTo16BitData(dataPacket);

                // check if size is correct to be a actual data packet
                if (rawDataPacket.size() != SensorNames.size()) {
                    // size not correct
                    break;
                }

                // calculate size of data
                size_t dataSize = (rawDataPacket.size() / SensorNames.size());

                // convert raw data to actual sensorshort data
                convertedDataPacket = convertRawData(dataSize, rawDataPacket);

                // add converted and raw (int16 format) sensorshort data to datapacket
                for (uint j = 0; j < SensorNames.size(); j++) {
                    dataPacketArray.at(i).dataPacket.at(j).second.push_back(convertedDataPacket.at(j));
                    dataPacketArray.at(i).rawDataPacket.at(j).second.push_back(rawDataPacket.at(j));
                }

                if (dataPacketArray.at(i).dataPacket.at(0).second.size() >= PacketTreshold) {

                    // emit signal to let the system know new data is received
                    emit packetArrayFilled(dataPacketArray);

                    // remove data from vector to prevent overflow
                    for (uint z = 0; z < SensorNames.size(); z++) {
                        dataPacketArray.at(i).dataPacket.at(z).second.clear();
                        dataPacketArray.at(i).rawDataPacket.at(z).second.clear();
                    }
                }
                break;
            }
        }
    }
}

/* convert the 8 bit data to the needed 16 bit data size
 *  each piece of data consists of a pair of 2 8 bit raw data
 *  this is combined using a bitwise or operation with the second raw data shifted
 */
std::vector<qint16> Utilities::convert8BitTo16BitData(std::vector<quint8> dataPacket)
{
    std::vector<qint16> rawData;
    int16_t dataBuffer = 0x0;
    int combineIndex = 0;

    for (uint i = 0; i < (dataPacket.size()/2); i++) {

        dataBuffer = ((dataPacket.at(combineIndex) | (dataPacket.at(combineIndex + 1) << 8)));

        // add combined data to raw buffer
        rawData.push_back(dataBuffer);

        // go to next pair of data
        combineIndex += 2;
    }
    return rawData;
}

std::vector<double> Utilities::convertRawData(size_t dataSize, std::vector<qint16> rawData)
{
    // initialize matrix array and returnvalue std::vector
    long double data[dataSize][SensorNames.size()];
    std::vector<double> dataOut;

    // if parameters are invalid, return empty std::vector
    if (dataSize == 0 || rawData.empty())
    {
        return dataOut;
    }

    // loop trough all the datasamples to convert them
    for (size_t i = 0; i < dataSize; i++)
    {
        for (size_t j = 0; j < SensorNames.size(); j++)
        {
            // reformat raw data in matrix array for easier converting
            data[i][j] = rawData.at(i * SensorNames.size() + j);
        }

        // TO-DO convert magic numbers

        // convert accelerometer raw data to real values
        data[i][0] = (data[i][0] / pow(2, 15)) * 32;
        data[i][1] = (data[i][1] / pow(2, 15)) * 32;
        data[i][2] = (data[i][2] / pow(2, 15)) * 32;

        data[i][9] = (data[i][9] / pow(2, 15)) * 32;
        data[i][10] = (data[i][10] / pow(2, 15)) * 32;
        data[i][11] = (data[i][11] / pow(2, 15)) * 32;

        data[i][18] = (data[i][18] / pow(2, 15)) * 32;
        data[i][19] = (data[i][19] / pow(2, 15)) * 32;
        data[i][20] = (data[i][20] / pow(2, 15)) * 32;

        data[i][27] = (data[i][27] / pow(2, 15)) * 32;
        data[i][28] = (data[i][28] / pow(2, 15)) * 32;
        data[i][29] = (data[i][29] / pow(2, 15)) * 32;

        data[i][36] = (data[i][36] / pow(2, 15)) * 32;
        data[i][37] = (data[i][37] / pow(2, 15)) * 32;
        data[i][38] = (data[i][38] / pow(2, 15)) * 32;

        // convert gyroscope raw data to real values
        data[i][3] = (data[i][3] / pow(2, 15)) * 4000;
        data[i][4] = (data[i][4] / pow(2, 15)) * 4000;
        data[i][5] = (data[i][5] / pow(2, 15)) * 4000;

        data[i][12] = (data[i][12] / pow(2, 15)) * 4000;
        data[i][13] = (data[i][13] / pow(2, 15)) * 4000;
        data[i][14] = (data[i][14] / pow(2, 15)) * 4000;

        data[i][21] = (data[i][21] / pow(2, 15)) * 4000;
        data[i][22] = (data[i][22] / pow(2, 15)) * 4000;
        data[i][23] = (data[i][23] / pow(2, 15)) * 4000;

        data[i][30] = (data[i][30] / pow(2, 15)) * 4000;
        data[i][31] = (data[i][31] / pow(2, 15)) * 4000;
        data[i][32] = (data[i][32] / pow(2, 15)) * 4000;

        data[i][39] = (data[i][39] / pow(2, 15)) * 4000;
        data[i][40] = (data[i][40] / pow(2, 15)) * 4000;
        data[i][41] = (data[i][41] / pow(2, 15)) * 4000;

        // convert magnetometer raw data to real values
        data[i][6] = data[i][6] * 0.15;
        data[i][7] = data[i][7] * 0.15;
        data[i][8] = data[i][8] * 0.15;

        data[i][15] = data[i][15] * 0.15;
        data[i][16] = data[i][16] * 0.15;
        data[i][17] = data[i][17] * 0.15;

        data[i][24] = data[i][24] * 0.15;
        data[i][25] = data[i][25] * 0.15;
        data[i][26] = data[i][26] * 0.15;

        data[i][33] = data[i][33] * 0.15;
        data[i][34] = data[i][34] * 0.15;
        data[i][35] = data[i][35] * 0.15;

        data[i][42] = data[i][42] * 0.15;
        data[i][43] = data[i][43] * 0.15;
        data[i][44] = data[i][44] * 0.15;

    }

    // add real values to correct formatted std::vector for making graphs
    for (size_t i = 0; i < SensorNames.size(); i++)
    {
        for (size_t j = 0; j < dataSize; j++)
        {
            dataOut.push_back(data[j][i]);
        }
    }
    // return the correctly formatted std::vector with all converted IMU data
    return dataOut;
}

