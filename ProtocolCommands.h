#ifndef PROTOCOLCOMMANDS_H
#define PROTOCOLCOMMANDS_H

#include <string>

// commands to receive
const char StartIdentifier = '{';
const char EndIdentifier = '}';

const char DataIdentifier = 'D';
const char InitIdentifier = 'I';

// commands to send
const std::string startCommand = "Start";
const std::string stopCommand = "Stop";
const std::string connectedCommand = "Connected";

#endif // PROTOCOLCOMMANDS_H
