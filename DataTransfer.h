#ifndef DATATRANSFER_H
#define DATATRANSFER_H

#include <cmath>
#include <string>
#include <fstream>
#include <vector>
#include <utility>   // std::pair
#include <stdexcept> // std::runtime_error
#include <sstream>   // std::stringstream

#include "Data.h"

class DataTransfer
{
public:
    DataTransfer() {}
    ~DataTransfer() {}

    /* read raw data and convert it to actual IMU values */
    std::vector<std::pair<std::string, std::vector<double>>> readData(std::string fileName = "RUN_1.TXT");

    /* read from csv file */
    std::vector<std::pair<std::string, std::vector<double>>> read_csv(std::ifstream &str);

    /* write from csv file */
    void write_csv_data(std::string filenameConverted, std::string filenameint16Format, std::vector<Data> dataset, int selectedShort);

    void write_csv_header(std::string filename, std::vector<std::string> dataset);

private:
    
    /*  */
    std::vector<std::pair<std::string, std::vector<double>>> convertRawData(size_t dataSize, std::vector<int16_t> rawData);

    /*  */
    std::vector<int16_t> getRawData(std::string fileName = "RUN_1.TXT");

    /* first 8 bytes in raw data are used for the magnetometer offset so these can be skipped */
    const size_t OffSet = 8;

    /* name vector of the different sensors used */
    const std::vector<std::string> sensorNames = {"acc-x-31", "acc-y-31", "acc-z-31", "gyr-x-31", "gyr-y-31", "gyr-z-31",
                                                  "mag-x-31", "mag-y-31", "mag-z-31", "acc-x-11", "acc-y-11", "acc-z-11",
                                                  "gyr-x-11", "gyr-y-11", "gyr-z-11", "mag-x-11", "mag-y-11", "mag-z-11",
                                                  "acc-x-21", "acc-y-21", "acc-z-21", "gyr-x-21", "gyr-y-21", "gyr-z-21",
                                                  "mag-x-21", "mag-y-21", "mag-z-21", "acc-x-12", "acc-y-12", "acc-z-12",
                                                  "gyr-x-12", "gyr-y-12", "gyr-z-12", "mag-x-12", "mag-y-12", "mag-z-12",
                                                  "acc-x-22", "acc-y-22", "acc-z-22", "gyr-x-22", "gyr-y-22", "gyr-z-22",
                                                  "mag-x-22", "mag-y-22", "mag-z-22", "time1", "time2", "movement"};

};

#endif
