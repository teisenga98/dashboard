/* base taken and modified from https://stackoverflow.com/questions/20546750/qtcpsocket-reading-and-writing */

#include "ProtocolCommands.h"
#include "server.h"

Server::Server(QObject *parent) : QObject(parent)
{

}

void Server::run()
{
    server = new QTcpServer(this);
    connect(server, SIGNAL(newConnection()), SLOT(newConnection()));
    qDebug() << "Listening:" << server->listen(QHostAddress::Any, 1024);
    qDebug() << QHostAddress::Any;
}

void Server::start()
{

}

void Server::stop()
{

}

void Server::newConnection()
{
    while (server->hasPendingConnections())
    {
        QTcpSocket *socket = server->nextPendingConnection();
        connect(socket, SIGNAL(readyRead()), SLOT(readyRead()));
        connect(socket, SIGNAL(disconnected()), SLOT(disconnected()));
        QByteArray *buffer = new QByteArray();
        qint32 *s = new qint32(0);
        buffers.insert(socket, buffer);
        sizes.insert(socket, s);
        socketList.push_back(socket);

        // send "{Connected}" to the sensorshort to let it know it can send UID and initialize
        std::string tmpConnectedMsg = StartIdentifier + connectedCommand + EndIdentifier;
        socket->write(tmpConnectedMsg.c_str(), tmpConnectedMsg.size());
        socket->waitForBytesWritten();
    }
}

void Server::disconnected()
{
    QTcpSocket *socket = static_cast<QTcpSocket*>(sender());
    QByteArray *buffer = buffers.value(socket);
    qint32 *s = sizes.value(socket);
    socket->deleteLater();
    delete buffer;
    delete s;

    for (int i = 0; i < socketList.size(); i++) {
        if(socketList.at(i) == socket)
        {
            emit removeShortFromInitializedList(socket->socketDescriptor());
            socketList.removeAt(i);
        }
    }
}

void Server::sendMessage(std::string message)
{
    for (int i = 0; i < socketList.size(); i++) {
        socketList.at(i)->write(message.c_str(), message.size());
        bool bytesWritten = socketList.at(i)->waitForBytesWritten();
        if (!bytesWritten) {
            // error bytes not written
        }
    }
}

void Server::readyRead()
{
    QTcpSocket *socket = static_cast<QTcpSocket*>(sender());
    QByteArray *buffer = buffers.value(socket);

    while (socket->bytesAvailable() > 0)
    {
        buffer->append(socket->readAll());
        QByteArray data(QByteArray::fromRawData(buffer->data(), buffer->size()));
        messageVector.push_back(data);

        // program doesn't work without calling data.data() first, no clue why
        data.data();

        buffer->remove(0, buffer->size());
        emit dataReceived(socket->socketDescriptor(), data);
    }
}
