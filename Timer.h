/**
        timer.h
        Purpose: creates a QTimer with a automatic timeout
        using signals
        @author Thomas Eisenga
        @version 1.0 27/07/2021
*/

#ifndef TIMER_H
#define TIMER_H

#include <QTimer>


class Timer : public QObject
{
    Q_OBJECT
public:
    Timer(QObject *parent = nullptr);
    ~Timer();

    Timer(const Timer &) = delete;
    Timer &operator =(const Timer &) = delete;

    /**
     * Start the timer
     */
    void start();

    /**
     * Stop the timer
     */
    void stop();

    /**
     * Get the remaining time
     * @return remaining time in milliseconds
     */
    int getRemainingTime() { return timer->remainingTime(); }

    /**
     * Sets the timeout for the QTimer
     * @param milliseconds time of timeout in milliseconds
     */
    void setTimeout(int milliseconds) { timer->setInterval(milliseconds); }

    /**
     * Sets the single shot for the QTimer
     * @param singleShot set the timer to single shot, default is false
     */
    void setSingleShot(bool singleShot = false) {timer->setSingleShot(singleShot); }

signals:
    void timeoutSignal();

private:
    void handleTimeout();

    QTimer *timer;
    int ms;

};

#endif // TIMER_H
