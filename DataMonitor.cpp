﻿#include <fstream>

#include "DataMonitor.h"
#include "ProtocolCommands.h"

DataMonitor::DataMonitor(QObject *parent):
    QObject(parent),
    updateGraphsTimer(new Timer()),
    utils(new Utilities()),
    server(new Server()),
    dataIndex(0),
    graphCount(0),
    selectedShort(0),
    systemStarted(false),
    systemPaused(false),
    systemStopped(true),
    systemRunsNr(1),
    filename("runs/RUN_"),
    staticDataset{},
    shortTimeout(0),
    longTimeout(0)
{

}

DataMonitor::~DataMonitor()
{
    delete updateGraphsTimer;
    updateGraphsTimer = nullptr;

    delete utils;
    utils = nullptr;
    delete server;
    server = nullptr;

    for (int i = 0; i < ChartAmount; i++) {
        delete sensorSeries.at(i);
        delete sensorCharts.at(i);
        sensorSeries.at(i) = nullptr;
        sensorCharts.at(i) = nullptr;
    }
}

bool DataMonitor::initialize(std::string fileName, int updateGraphsTimerTimeout)
{
    // check if the parameters are valid
    if (fileName.empty() || updateGraphsTimerTimeout < MinTimeout) {
        return false;
    }

    QThread *serverThread = new QThread();

    server->moveToThread(serverThread);

    // connect server signal to utilities signal
    QObject::connect(server, &Server::dataReceived, utils, &Utilities::checkValidMessage);

    QObject::connect(server, &Server::removeShortFromInitializedList, utils, &Utilities::removeShortFromList);

    QObject::connect(serverThread, &QThread::started, server, &Server::run, Qt::QueuedConnection);

    // connect utilities signal to correct slot for receiving new data
    QObject::connect(utils, SIGNAL(packetArrayFilled(std::vector<Data>)), this, SLOT(newDataPacketArraySlot(std::vector<Data>)));

    QObject::connect(utils, SIGNAL(newSensorShortInitialized()), this, SLOT(newSensorShortConnectedSlot()));

    // open and read file
    staticDataset = transfer.readData(fileName);
    //    data = transfer.read_csv(file);

    // check if data is valid
    if (staticDataset.empty()) {
        return false;
    }

    // initialize timer and connect it to correct slot
    updateGraphsTimer->setTimeout(updateGraphsTimerTimeout);
    QObject::connect(updateGraphsTimer, SIGNAL(timeoutSignal()), this, SLOT(updateGraphsTimerSlot()));

    for (int i = 0; i < ChartAmount; i++) {
        sensorGraphs.push_back(new QBarSet(""));
    }

    // add initial data to the charts
    for (int j = 0; j < ChartAmount; ++j) {
        sensorGraphs.at(j)->append(0);
        sensorGraphs.at(j)->setColor(QColor(0, 166, 214)); // TU Delft blue
    }

    // add charts to horizontal bar series
    for (int i = 0; i < ChartAmount; i++) {
        sensorSeries.push_back(new QHorizontalBarSeries());
        sensorSeries.at(i)->append(sensorGraphs.at(i));
    }

    // create QChart and add horizontal bar series to it
    for (int i = 0; i < ChartAmount; i++) {
        sensorCharts.push_back(new QChart());
        sensorCharts.at(i)->addSeries(sensorSeries.at(i));
        sensorCharts.at(i)->setAnimationOptions(QChart::SeriesAnimations);
    }


    // set axis pixel size
    QFont labelFont;
    labelFont.setPixelSize(12);

    // create and add axis to charts
    std::vector<QBarCategoryAxis*> sensorYAxis;
    std::vector<QValueAxis*> sensorXAxis;

    // create QValueAxis for each chart
    for (int i = 0; i < ChartAmount; i++) {
        sensorXAxis.push_back(new QValueAxis());
    }

    // add minimum and maximum values to sensorXAxis
    sensorXAxis.at(0)->setRange(MinAccValue, MaxAccValue);
    sensorXAxis.at(1)->setRange(MinGyrValue, MaxGyrValue);
    sensorXAxis.at(2)->setRange(MinMagValue, MaxMagValue);

    sensorXAxis.at(3)->setRange(MinAccValue, MaxAccValue);
    sensorXAxis.at(4)->setRange(MinGyrValue, MaxGyrValue);
    sensorXAxis.at(5)->setRange(MinMagValue, MaxMagValue);

    sensorXAxis.at(6)->setRange(MinAccValue, MaxAccValue);
    sensorXAxis.at(7)->setRange(MinGyrValue, MaxGyrValue);
    sensorXAxis.at(8)->setRange(MinMagValue, MaxMagValue);

    for (int i = 0; i < ChartAmount; i++) {
        sensorYAxis.push_back(new QBarCategoryAxis());

        sensorYAxis.at(i)->append("");

        sensorCharts.at(i)->addAxis(sensorYAxis.at(i), Qt::AlignLeft);
        sensorCharts.at(i)->addAxis(sensorXAxis.at(i), Qt::AlignBottom);

        sensorYAxis.at(i)->setLabelsFont(labelFont);
        sensorXAxis.at(i)->setLabelsFont(labelFont);

        sensorSeries.at(i)->attachAxis(sensorYAxis.at(i));
        sensorSeries.at(i)->attachAxis(sensorXAxis.at(i));
    }

    // hide legend for charts
    for (int i = 0; i < ChartAmount; i++) {
        sensorCharts.at(i)->legend()->hide();
    }

    serverThread->start();;

    return true;
}

void DataMonitor::start()
{
    // only if the system has not started already, start the system
    if (!systemStarted) {

        // get the connected and initialized sensorshorts
        int initializedShortsAmount = getConnectedsensorShortsAmount();

        // if system has stopped and there are connected shorts a new run has started, write names again
        if (systemStopped && initializedShortsAmount != 0) {

            for (int i = 0; i < initializedShortsAmount; i++) {

                std::vector<int> tmp = getConnectedSensorShorts();

                // create correct naming for files
                std::string convertedFileName = filename + std::to_string(systemRunsNr) + "_short_" + std::to_string(tmp.at(i)) + "_converted.csv";
                std::string int16FormatFileName = filename + std::to_string(systemRunsNr) + "_short_" + std::to_string(tmp.at(i)) + "_int16format.csv";

                // write header names to file using sensorNames
                transfer.write_csv_header(convertedFileName, SensorNames);
                transfer.write_csv_header(int16FormatFileName, SensorNames);
            }
        }

        // tell the ui that the system can start and send true if the system is using actual sensordata
        emit systemStartedSignal((initializedShortsAmount != 0));

        // set the start stop pause booleans accordingly
        systemStarted = true;
        systemPaused = false;
        systemStopped = false;

        // only start graph timer if not using real sensordata
        if (initializedShortsAmount == 0) {
            // start the timer for updating the graphs
            updateGraphsTimer->start();
        }
        else {
            // send the start command to the connected sensorshorts
            server->sendMessage(StartIdentifier + startCommand + EndIdentifier);
        }

    }
}

void DataMonitor::pause()
{
    // only if the system has not been paused already, pause the system
    if (systemStarted && !systemPaused && !systemStopped) {

        // set the start pause booleans accordingly
        systemStarted = false;
        systemPaused = true;

        // tell the ui that the system is stopped and send true if the system is using actual sensordata
        emit systemPausedSignal((getConnectedsensorShortsAmount() != 0));

        // stop the timer for updating the graphs
        updateGraphsTimer->stop();
    }
}

void DataMonitor::stop()
{
    // only if the system has not been stopped already, stop the system
    if (!systemStopped) {

        // set the start stop pause booleans accordingly
        systemStarted = false;
        systemPaused = false;
        systemStopped = true;

        // get the connected and initialized sensorshorts
        int initializedShortsAmount = getConnectedsensorShortsAmount();

        // tell the ui that the system is stopped and send true if the system is using actual sensordata
        emit systemStoppedSignal((initializedShortsAmount != 0));

        // only stop graph timer if not using real sensordata
        if (initializedShortsAmount == 0) {
            // stop the timer for updating the graphs
            updateGraphsTimer->stop();
        }
        else {

            // update run number counter to setup a new run
            systemRunsNr++;

            // send the stop command to the connected sensorshorts
            server->sendMessage(StartIdentifier + stopCommand + EndIdentifier);
        }

        // reset data index for static data
        dataIndex = 0;
    }
}

void DataMonitor::shortSelected(int sensorNr)
{
    selectedShort = sensorNr - 1; // array start at index 0
}

void DataMonitor::newDataPacketArraySlot(std::vector<Data> packetArrayIn)
{
    // only use incoming data if the system has started
    if (systemStarted && !systemPaused) {

        // get the connected and initialized sensorshorts
        int initializedShortsAmount = getConnectedsensorShortsAmount();

        // only use incoming data if a sensorshort is connected
        if (initializedShortsAmount > 0) {

            std::vector<Data> packetArray = packetArrayIn;

            // extra iterator for getting the X Y Z values of each sensor
            int j = 0;

            // check if there is data in the selected short, if not skip section
            if (!packetArray.at(selectedShort).dataPacket.empty()) {

                // loop trough all connected shorts and save its data to the correct files
                for (int i = 0; i < initializedShortsAmount; i++) {

                    std::vector<int> tmp = getConnectedSensorShorts();

                    // create correct naming for files
                    std::string convertedFileName = filename + std::to_string(systemRunsNr) + "_short_" + std::to_string(tmp.at(i)) + "_converted.csv";
                    std::string int16FormatFileName = filename + std::to_string(systemRunsNr) + "_short_" + std::to_string(tmp.at(i)) + "_int16format.csv";

                    // write converted and int16 format data to files
                    transfer.write_csv_data(convertedFileName, int16FormatFileName, packetArray, tmp.at(i) - 1); // array start at index 0 so -1 is needed
                }

                // loop trough all charts and add correct value to it using Euclidean norm: square root (√) of ( (x-axis)^2 + (y-axis)^2 + (z-axis)^2 )
                for (int i = 0; i < ChartAmount; i++) {

                    for (uint z = 0; z < packetArray.at(selectedShort).dataPacket.at(0).second.size(); z++) {

                        double euclidean = sqrt((pow(packetArray.at(selectedShort).dataPacket.at(j).second.at(z), 2) +
                                                 pow(packetArray.at(selectedShort).dataPacket.at(j+1).second.at(z), 2) +
                                                 pow(packetArray.at(selectedShort).dataPacket.at(j+2).second.at(z), 2)));

                        // get the correct color for the selected graph
                        QColor graphColor = checkTreshold(euclidean, i);

                        // set the selected graph to the right color according to treshold
                        sensorGraphs.at(i)->setColor(graphColor);

                        // set new value to graph UI
                        sensorGraphs.at(i)->replace(0, euclidean);
                    }

                    // go to next sensor
                    j+=3;
                }
            }
        }
    }
}

void DataMonitor::updateGraphsTimerSlot()
{
    // only use sample data if the system has started
    if (systemStarted && !systemPaused) {

        // only use sample data if no sensorshort is connected
        if (getConnectedsensorShortsAmount() == 0) {

            if (dataIndex < MaxDemoData) {
                dataIndex++;
            }
            else {
                dataIndex = 0;
            }

            // extra iterator for getting the X Y Z values of each sensor
            int j = 0;

            // loop trough all charts and add correct value to it using Euclidean norm: square root (√) of ( (x-axis)^2 + (y-axis)^2 + (z-axis)^2 )
            for (int i = 0; i < ChartAmount; i++) {
                double euclidean = sqrt((pow(staticDataset.at(j).second.at(dataIndex), 2) +
                                         pow(staticDataset.at(j+1).second.at(dataIndex), 2) +
                                         pow(staticDataset.at(j+2).second.at(dataIndex), 2)));

                // get the correct color for the selected graph
                QColor graphColor = checkTreshold(euclidean, i);

                // set the selected graph to the right color according to treshold
                sensorGraphs.at(i)->setColor(graphColor);

                // set new value to graph UI
                sensorGraphs.at(i)->replace(0, euclidean);

                // go to next sensor
                j+=3;
            }
        }
    }
}

QColor DataMonitor::checkTreshold(double euclideanIn, int index)
{
    QColor color(0, 166, 214); // TU Delft blue

    // check which sensor is selected to use the correct treshold
    if (index == trunkAccVecLocation || index == leftLegAccVecLocation || index == rightLegAccVecLocation) {

        // if value is over treshold change color of graph to 'notify' the user the value is high
        if (euclideanIn > accValueTreshold) {
            color = Qt::red;
        }
    }
    else if (index == trunkGyrVecLocation || index == leftLegGyrVecLocation || index == rightLegGyrVecLocation) {

        // if value is over treshold change color of graph to 'notify' the user the value is high
        if (euclideanIn > GyrValueTreshold) {
            color = Qt::red;
        }
    }
    else if (index == trunkMagVecLocation || index == leftLegMagVecLocation || index == rightLegMagVecLocation) {

//         if the current value is of magnetometer value multiply the value to show in Microtesla instead of Tesla
//        euclideanIn = euclideanIn * TeslaMicroTeslaConversion;

        // if value is over treshold change color of graph to 'notify' the user the value is high
        if (euclideanIn > MagValueTreshold) {
            color = Qt::red;
        }
    }

    return color;
}
