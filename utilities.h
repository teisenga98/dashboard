#ifndef UTILITIES_H
#define UTILITIES_H

#include <QtCore>

#include "Data.h"

class Utilities: public QObject
{
    Q_OBJECT
public:
    Utilities(QObject *parent = nullptr);

    virtual ~Utilities() {}

    int getConnectedSensorShortsAmount();

    std::vector<int> getConnectedSensorShorts();

public slots:
    void checkValidMessage(quint8 socketID, QByteArray message);

    void removeShortFromList(quint8 socketID);

signals:
    void packetArrayFilled(std::vector<Data> dataPacketArray);

    void newSensorShortInitialized();

private:

    void checkMessageType(quint8 socketID, std::vector<quint8> dataPacket);

    std::vector<qint16> convert8BitTo16BitData(std::vector<quint8> dataPacket);

    std::vector<double> convertRawData(size_t dataSize, std::vector<qint16> rawData);

    bool startIdentified;
    bool startHalfIdentified;
    bool endIdentified;
    bool endHalfIdentified;

    quint8 sensorShortsAmount;

    // internal vector for the current data
    std::vector<quint8> dataPacket;

    // vector that keeps track of all the data
    std::vector<Data> dataPacketArray;

    // base name for sensor shorts
    const std::string SensorSHortName = "short ";

    // max amount of accepted sensorshorts at the time
    const quint8 MaxSensorShorts = 22;

    // first 8 bytes in raw data are used for the magnetometer offset so these can be skipped
    const size_t OffSet = 8;

    // this treshold determines the amount of data packets that have to be received before showing it on the display
    const size_t PacketTreshold = 100;

    std::vector<std::pair<int, bool>> connectedSensorShorts = { {1, false },
                                                                {2, false },
                                                                {3, false },
                                                                {4, false },
                                                                {5, false },
                                                                {6, false },
                                                                {7, false },
                                                                {8, false },
                                                                {9, false },
                                                                {10, false },
                                                                {11, false },
                                                                {12, false },
                                                                {13, false },
                                                                {14, false },
                                                                {15, false },
                                                                {16, false },
                                                                {17, false },
                                                                {18, false },
                                                                {19, false },
                                                                {20, false },
                                                                {21, false },
                                                                {22, false } };

};

#endif // UTILITIES_H
